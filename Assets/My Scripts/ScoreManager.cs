﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

    public int moves, target;
    int score;

    public GameObject GUI_Target,GUI_Score,GUI_Moves;

	// Use this for initialization
	void Start ()
    {
	    score = 0;
        GUI_Target.guiText.text = target.ToString();
        GUI_Score.guiText.text = score.ToString();
        GUI_Moves.guiText.text = moves.ToString();
	}
	
    public void addPoints(int points)
    {
        score += points;
        GUI_Score.guiText.text = score.ToString();
       
        
    }

    public void moveDecrement()
    {
        moves--;
       

        if (moves == 0 && score < target)
        {
            PlayerPrefs.SetInt("victory", 0);
            Application.LoadLevel("Menu End");            
        }
        else if ( score >= target)
        {
            PlayerPrefs.SetInt("victory", 1);
            Application.LoadLevel("Menu End");  
        }
        GUI_Moves.guiText.text = moves.ToString();
    }

	// Update is called once per frame
	void Update () 
    {
	   
	}
}
