﻿using UnityEngine;
using System.Collections;

public class SwipeDetector : MonoBehaviour
{

    public static Vector2 fp, lp;

    public enum SwipeTypes
    {
        None, LeftSwipe, RightSwipe, UpSwipe, DownSwipe
    }

    public static SwipeTypes currentSwipeType;
    // Use this for initialization
    void Start()
    {
        Input.multiTouchEnabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        currentSwipeType = SwipeTypes.None;

        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            handleMouse();
        }
        else if (Application.platform == RuntimePlatform.WP8Player)
        {
            handleTouch();
        }


        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        // Debug.LogError(currentSwipeType);
    }


    void handleMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            fp = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            lp = Input.mousePosition;
            currentSwipeType = getSwipe(getAngle());
        }
    }

    void handleTouch()
    {
        foreach (Touch t in Input.touches)
        {
            if (t.phase == TouchPhase.Began)
            {
                fp = t.position;
            }
            else if (t.phase == TouchPhase.Ended)
            {
                lp = t.position;
                currentSwipeType = getSwipe(getAngle());
            }
        }
    }


    float getAngle()
    {
        float angle = float.NaN;
        if (Vector2.Distance(fp, lp) > 15)
        {
            Vector2 dir = lp - fp;
            angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

            if (angle < 0)
            {
                angle = 360 - Mathf.Abs(angle);
            }
        }
        return angle;
    }

    SwipeTypes getSwipe(float angle)
    {
        if ((angle < 30 && angle > 0) || (angle < 360 && angle > 330))
        {
            return SwipeTypes.LeftSwipe;
        }
        else if (angle > 150 && angle < 210)
        {
            return SwipeTypes.RightSwipe;
        }
        else if (angle > 30 && angle < 120)
        {
            return SwipeTypes.UpSwipe;
        }
        else if (angle > 240 && angle < 300)
        {
            return SwipeTypes.DownSwipe;
        }

        return SwipeTypes.None;
    }
}
