﻿using UnityEngine;
using System.Collections;

public class ResultChecker : MonoBehaviour {

    public Sprite loseSprite, winSprite, RetryButton, playAgainButton;
    public GameObject imageTemplate,buttonTemplate;
    public GUIText msg;
	// Use this for initialization
	void Start () 
    {
	    if(PlayerPrefs.GetInt("victory")==0)
        {
            imageTemplate.GetComponent<SpriteRenderer>().sprite = loseSprite;
            msg.guiText.text = "YOU LOSE!";
            buttonTemplate.GetComponent<SpriteRenderer>().sprite = RetryButton;
        }
        else
        {
            imageTemplate.GetComponent<SpriteRenderer>().sprite = winSprite;
            msg.guiText.text = "YOU WIN!";
            buttonTemplate.GetComponent<SpriteRenderer>().sprite = playAgainButton;
        }
	}	
	
}
