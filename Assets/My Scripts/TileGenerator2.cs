﻿using UnityEngine;
using System.Collections;
using System;

public class TileGenerator2 : MonoBehaviour {


    public int tileXSize, tileYSize;
    public GameObject tileTemplate;
    public Sprite[] tileSprites;
    public float offset, tileSize;
    public Sprite TileBackTL,TileBackBL,TileBackTR,TileBackBR,TileBack;

    //sounds
    public AudioClip swapSound,chompSound;

    //ScoreManager
    public GameObject scoreManager;


    int[,] dataGrid, markerGrid;
    GameObject[,] spriteGrid;

    bool swapping = false;
    bool processing = false;

	// Use this for initialization
	void Start ()
    {
        //initialize
        dataGrid = new int[tileXSize, tileYSize+1];
        markerGrid = new int[tileXSize, tileYSize];
        spriteGrid = new GameObject[tileXSize, tileYSize+1];
        makeTileGrid();
	}


    float getDis(float x, float y)
    {
        if (x > y)
        {
            return Mathf.Abs(x - y);
        }
        else
        {
            return Mathf.Abs(y - x);
        }
    }
	// Update is called once per frame
	void Update () 
    {
       
        
        if (SwipeDetector.currentSwipeType != SwipeDetector.SwipeTypes.None)
        {
            Vector2 firstPos = Camera.main.ScreenToWorldPoint(SwipeDetector.fp);

            if (firstPos.x >= gameObject.transform.position.x && firstPos.x <= (gameObject.transform.position.x + (spriteGrid.GetLength(0) * offset))
                && firstPos.y >= gameObject.transform.position.y && firstPos.y <= (gameObject.transform.position.y + (spriteGrid.GetLength(1) * offset))
                && !swapping)
            {
                int x = (int)(getDis(firstPos.x, gameObject.transform.position.x) / offset);
                int y = (int)(getDis(firstPos.y, gameObject.transform.position.y) / offset);


                if (SwipeDetector.currentSwipeType == SwipeDetector.SwipeTypes.LeftSwipe)
                    StartCoroutine(swapAnim(x, y, x + 1, y, 0.5f, true));
                else if (SwipeDetector.currentSwipeType == SwipeDetector.SwipeTypes.RightSwipe)
                    StartCoroutine(swapAnim(x, y, x - 1, y, 0.5f, true));
                else if (SwipeDetector.currentSwipeType == SwipeDetector.SwipeTypes.UpSwipe)
                    StartCoroutine(swapAnim(x, y, x, y + 1, 0.5f, true));
                else if (SwipeDetector.currentSwipeType == SwipeDetector.SwipeTypes.DownSwipe)
                    StartCoroutine(swapAnim(x, y, x, y - 1, 0.5f, true));                
            }
        }  
	}

    Vector2 calcPos(int x, int y)
    {
        return new Vector2(gameObject.transform.position.x + (offset * x) + (offset * 0.5f), gameObject.transform.position.y + (offset * y) + (offset * 0.5f));
    }
    void makeTileGrid()
    {
        Vector2 size = new Vector2(tileSize, tileSize);
       
        int r;
        for (int y = 0; y < tileYSize; y++)
        {
            for (int x = 0; x < tileXSize; x++)
            {
                do
                {
                    r = UnityEngine.Random.Range(0, tileSprites.Length);
                }
                while ((x >= 2 && dataGrid[x - 1, y] == r && dataGrid[x - 2, y] == r)
                    ||
                     (y >= 2 && dataGrid[x, y - 1] == r && dataGrid[x, y - 2] == r));


                dataGrid[x, y] = r;

                GameObject t = (GameObject)Instantiate(tileTemplate, calcPos(x, y), Quaternion.identity);

                t.GetComponent<SpriteRenderer>().sprite = tileSprites[r];
                t.transform.parent = gameObject.transform;
                t.transform.localScale = size;
                spriteGrid[x, y] = t;
                dataGrid[x, y] = r;


                //make tile backs
                
                GameObject tb = (GameObject)Instantiate(tileTemplate, calcPos(x, y), Quaternion.identity);
                tb.transform.parent = gameObject.transform;
                tb.transform.localScale = size;
                SpriteRenderer sr =  tb.GetComponent<SpriteRenderer>();
                sr.sortingLayerName = "TileBack";
                if (x == 0 && y==0) { sr.sprite = TileBackBL; }
                else if (x == tileXSize-1 && y == 0) { sr.sprite = TileBackBR; }
                else if (x == 0 && y == tileYSize - 1) { sr.sprite = TileBackTL; }
                else if (x == tileXSize - 1 && y == tileYSize - 1) { sr.sprite = TileBackTR; }
                else { sr.sprite = TileBack; }
            }
        }
    }

    IEnumerator swapAnim(int x1, int y1, int x2, int y2, float duration, bool checkForMatch)
    {
        
        swapping = true;
        GameObject blockA = null;
        GameObject blockB = null;
        
        if(y1==tileYSize || y2==tileYSize)
        {
            swapping = false;
            yield break;
        }
        try
        {
            blockA = spriteGrid[x1, y1];
            blockB = spriteGrid[x2, y2];
        }
        catch (IndexOutOfRangeException ex)
        {
            Debug.Log(ex.Message);
            swapping = false;
            yield break;
        }

        if (blockA == null || blockB == null)
        {
            swapping = false;
            yield break;
        }

        if(swapSound!=null)
        {
            AudioSource.PlayClipAtPoint(swapSound, Vector3.zero);
        }

        for (float t = 0.0f; t < duration; t += Time.deltaTime)
        {
            blockA.transform.position = Vector2.Lerp(calcPos(x1, y1), calcPos(x2, y2), t / duration);
            blockB.transform.position = Vector2.Lerp(calcPos(x2, y2), calcPos(x1, y1), t / duration);
            yield return 0;
        }


        spriteGrid[x1, y1] = blockB;
        spriteGrid[x2, y2] = blockA;

        int temp = dataGrid[x1, y1];
        dataGrid[x1, y1] = dataGrid[x2, y2];
        dataGrid[x2, y2] = temp;

        if (checkForMatch)
        {
            if (!MatcheExists())
            {
                yield return StartCoroutine(swapAnim(x1, y1, x2, y2, duration, false));
            }
            else
            {
                
                do
                {                    
                    deleteTiles();
                    yield return new WaitForSeconds(0.1f);
                    AudioSource.PlayClipAtPoint(chompSound, Vector3.zero);
                    newTile();
                    yield return new WaitForSeconds(0.5f);                  

                }
                while (MatcheExists());
                scoreManager.GetComponent<ScoreManager>().moveDecrement();
            }
        }
        swapping = false;        
    }


    void deleteTiles()
    {
        int points=0;
        for (int y = 0; y < tileYSize; y++)
        {
            for (int x = 0; x < tileXSize; x++)
            {
                if (markerGrid[x, y] == 1)
                {                   
                    GameObject a = spriteGrid[x, y];                    
                    spriteGrid[x, y] = null;
                    Destroy(a);
                    dataGrid[x, y] = -1;
                    points += 10;
                }

            }
        }
        scoreManager.GetComponent<ScoreManager>().addPoints(points);
    }
    bool MatcheExists()
    {
        bool matchExists = false;
        for (int y = 0; y < tileYSize; y++)
        {
            for (int x = 0; x < tileXSize; x++)
            {
                if (isRemovable(x, y, dataGrid[x, y]))
                {
                    markerGrid[x, y] = 1;
                    matchExists = true;
                }
                else
                {
                    markerGrid[x, y] = 0;
                }

            }
        }

//        deleteTiles();
        return matchExists;
    }
    bool isRemovable(int x, int y, int tileType)
    {
        //if an empty tile
        if (tileType == -1)
        {
            return false;
        }

        int horiz = 1;
        //left horizontal check
        for (int i = x - 1; i >= 0; i--)
        {
            if (dataGrid[i, y] == tileType) { horiz++; }
            else { break; }
        }

        //right horizontal check
        for (int i = x + 1; i < tileXSize; i++)
        {
            if (dataGrid[i, y] == tileType) { horiz++; }
            else { break; }
        }

        int vertiz = 1;
        //down vertical check
        for (int i = y - 1; i >= 0; i--)
        {
            if (dataGrid[x, i] == tileType) { vertiz++; }
            else { break; }
        }

        //up vertical check
        for (int i = y + 1; i < tileYSize; i++)
        {
            if (dataGrid[x, i] == tileType) { vertiz++; }
            else { break; }
        }

        if (horiz >= 3 || vertiz >= 3)
            return true;
        else
            return false;
    }


    void newTile()
    {
        bool matched = true;       
        while(matched)
        {
            matched = false;
            for (int x = 0; x < tileXSize; x++)
            {
                for (int y = 0; y < tileYSize; y++)
                {
                    if (dataGrid[x, y] == -1)
                    {
                        AddnewTile(x, y);
                        goDownColumn(x, y);                        
                        matched = true;
                        break;
                    }
                }
            }
        }      
        processing = false;
    }

    void AddnewTile(int x, int holeIndex)
    {
      
        int randomNumber = UnityEngine.Random.Range(0, tileSprites.Length);
       
        Vector2 pos = gameObject.transform.position;
        Vector2 scale = new Vector2(tileSize, tileSize);
        GameObject tile = (GameObject)Instantiate(tileTemplate, calcPos(x,tileYSize), Quaternion.identity);
        tile.GetComponent<SpriteRenderer>().sprite = tileSprites[randomNumber];
        tile.transform.localScale = scale;
        tile.transform.parent = gameObject.transform;

        dataGrid[x, tileYSize] = randomNumber;
        spriteGrid[x, tileYSize] = tile;
    }
    void goDownColumn(int x, int holeindex)
    {
        for (int y = holeindex; y < dataGrid.GetLength(1)-1; y++)
        {
            if (spriteGrid[x, y+1] != null)
            {
                StartCoroutine(moveDown(x, y+1, 0.1f));
            }
        }

        //update data
        for (int y = holeindex; y < dataGrid.GetLength(1) - 1; y++)
        {
            dataGrid[x, y] = dataGrid[x, y + 1];
            spriteGrid[x, y] = spriteGrid[x, y + 1];
        }

    }
    IEnumerator moveDown(int x, int y, float duration)
    {

        GameObject block = spriteGrid[x, y];       
        for (float t = 0.0f; t < duration; t += Time.deltaTime)
        {
            block.transform.position = Vector2.MoveTowards(calcPos(x,y),calcPos(x,y-1), t / duration);
            yield return 0;
        }   
    }

}
